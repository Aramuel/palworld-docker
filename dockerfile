FROM steamcmd/steamcmd:latest

ENV TZ=Etc/UTC
ARG DEBIAN_FRONTEND=noninteractive

RUN useradd -m manager

RUN apt-get update && apt-get install -y xdg-user-dirs xdg-utils && apt-get clean

RUN steamcmd +login anonymous +app_update 2394010 validate +quit

RUN mkdir /root/.steam/sdk64
RUN cp /root/.steam/steamcmd/linux64/steamclient.so /root/.steam/sdk64/steamclient.so

COPY backup-saves.sh /home/manager
COPY start-server.sh /home/manager
COPY scheduler.sh /home/manager

RUN mkdir /home/manager/backup

RUN chmod +x /home/manager/backup-saves.sh
RUN chmod +x /home/manager/start-server.sh
RUN chmod +x /home/manager/scheduler.sh

RUN chown -R manager:manager /root

USER manager

ENTRYPOINT ["/home/manager/start-server.sh"]