#!/bin/bash

# Set the source and destination directories
SOURCE_DIR="/root/.steam/SteamApps/common/PalServer/Pal/Saved/SaveGames/0"
BACKUP_DIR="/home/manager/backup"
MAX_BACKUPS=${MAX_SAVE_BACKUPS:-5}

# Create a timestamped backup
TIMESTAMP=$(date +"%Y%m%d-%H%M%S")
tar -czf "$BACKUP_DIR/save_backup_$TIMESTAMP.tar.gz" "$SOURCE_DIR"

# Remove old backups if the number exceeds MAX_BACKUPS
cd "$BACKUP_DIR"
if [ $(ls -t | grep save_backup | wc -l) -gt $MAX_BACKUPS ]; then
    ls -t | grep save_backup | tail -n +$(($MAX_BACKUPS + 1)) | xargs rm
fi
