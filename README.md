# Palworld docker

## Simple docker setup for running Palworld server

Created this quickly to run Palworld dedicated server.
I would highly recommend using docker-compose.

## Config

Regarding server configuration refer to [official wiki](https://tech.palworldgame.com/optimize-game-balance)

